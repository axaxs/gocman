package main

import (
	"bufio"
	"os"
	"strings"
)

type Config struct {
	options *Options
	repos   []*Repo
}

type Options struct {
	RootDir            string
	DBPath             string
	CacheDir           string
	LogFile            string
	GPGDir             string
	HoldPkg            []string
	Architecture       string
	IgnorePkg          []string
	IgnoreGroup        []string
	NoUpgrade          []string
	NoExtract          []string
	CheckSpace         bool
	SigLevel           []string
	LocalFileSigLevel  []string
	RemoteFileSigLevel []string
}

func parseValues(s string) (string, string) {
	var key, value string
	sp := strings.Split(s, "=")
	key = strings.TrimSpace(sp[0])
	if len(sp) > 1 {
		value = strings.TrimSpace(strings.Join(sp[1:], "="))
	}
	return key, value
}

func stringToList(s string) []string {
	hp := make([]string, 0, 2)
	for _, val := range strings.Split(s, " ") {
		if val != "" {
			hp = append(hp, val)
		}
	}
	return hp
}

func readConf() (*Config, error) {
	fi, err := os.Open("/etc/pacman.conf")
	if err != nil {
		return nil, err
	}
	defer fi.Close()
	br := bufio.NewScanner(fi)
	var section string
	config := &Config{}
	var repo *Repo
	for br.Scan() {
		newSection := false
		lt := strings.TrimSpace(br.Text())
		if lt == "" || lt[0] == '#' {
			continue
		}
		if strings.HasPrefix(lt, "[") && strings.HasSuffix(lt, "]") {
			section = strings.Trim(lt, "[]")
			newSection = true
		}
		if section == "options" {
			if config.options == nil {
				config.options = &Options{}
			}
			k, v := parseValues(lt)
			switch k {
			case "RootDir":
				config.options.RootDir = v
			case "DBPath":
				config.options.DBPath = v
			case "CacheDir":
				config.options.CacheDir = v
			case "LogFile":
				config.options.LogFile = v
			case "GPGDir":
				config.options.GPGDir = v
			case "HoldPkg":
				config.options.HoldPkg = stringToList(v)
			case "Architecture":
				config.options.Architecture = v
			case "IgnorePkg":
				config.options.IgnorePkg = stringToList(v)
			case "IgnoreGroup":
				config.options.IgnoreGroup = stringToList(v)
			case "NoUpgrade":
				config.options.NoUpgrade = stringToList(v)
			case "NoExtract":
				config.options.NoExtract = stringToList(v)
			case "CheckSpace":
				config.options.CheckSpace = true
			case "SigLevel":
				config.options.SigLevel = stringToList(v)
			case "LocalFileSigLevel":
				config.options.LocalFileSigLevel = stringToList(v)
			case "RemoteFileSigLevel":
				config.options.RemoteFileSigLevel = stringToList(v)
			}
		} else if newSection {
			if config.repos == nil {
				config.repos = make([]*Repo, 0, 3)
			}
			if repo != nil {
				config.repos = append(config.repos, repo)
			}
			repo = &Repo{}
			repo.Name = section
		} else {
			k, v := parseValues(lt)
			switch k {
			case "Include":
				repo.Include = v
			case "SigLevel":
				repo.SigLevel = stringToList(v)
			case "Server":
				repo.Server = v
			}
		}
	}
	if repo != nil {
		config.repos = append(config.repos, repo)
	}
	return config, nil
}
