package main

import (
	"archive/tar"
	//	"compress/gzip"
	"bytes"
	gzip "code.google.com/p/vitess/go/cgzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"
	"sync"
)

var localDB = make(PackageList, 0, 50)
var provMap = make(map[string]PackageList)

var wg sync.WaitGroup

func buildRepos() {
	wg.Add(1)
	go buildLocalDB()
	for _, v := range mainConfig.repos {
		wg.Add(1)
		go v.buildDB()
	}
	wg.Wait()
}

func buildProvides() {
	for _,v := range mainConfig.repos {
		for _,vv := range v.Packages {
			if vv.Provides == nil {
				continue
			}
			for _,vvv := range vv.Provides {
				pn := vvv.Name
				if _,ok := provMap[pn]; ok {
					provMap[pn] = append(provMap[pn], vv)
				} else {
					provMap[pn] = PackageList{vv}
				}
			}
		}
	}
	for _,v := range localDB {
		if v.Provides == nil {
			continue
		}
		for _,vv := range v.Provides {
			pn := vv.Name
                	if pk,ok := provMap[pn]; ok {
				if pk.containsPackage(v) {
					continue
				}
        	                provMap[pn] = append(pk, v)
                        } else {
                                provMap[pn] = PackageList{v}
                        }
		}
	}
}

func buildLocalDB() {
	defer wg.Done()
	fis, err := ioutil.ReadDir("/var/lib/pacman/local")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for _, v := range fis {
		p := &Package{}
		f, err := os.Open("/var/lib/pacman/local/" + v.Name() + "/desc")
		if err != nil {
			fmt.Println(err)
			continue
		}
		p.buildFromDBFile(f)
		localDB = append(localDB, p)
	}
}

func (r *Repo) buildDB() error {
	defer wg.Done()
	db := "/var/lib/pacman/sync/" + r.Name + ".db"
	fi, err := os.Open(db)
	if err != nil {
		return err
	}
	defer fi.Close()
	rd, err := gzip.NewReader(fi)
	if err != nil {
		return err
	}
	z, _ := ioutil.ReadAll(rd)
	rr := bytes.NewBuffer(z)
	tr := tar.NewReader(rr)
	var p *Package
	r.Packages = make([]*Package, 0, 10)
	c := 0
	for hdr, err := tr.Next(); err != io.EOF; hdr, err = tr.Next() {
		if strings.HasSuffix(hdr.Name, "/") {
			p = &Package{Repo: r}
			c = 0
		} else if strings.HasSuffix(hdr.Name, "/desc") {
			p.buildFromDBFile(tr)
			c++
		} else if strings.HasSuffix(hdr.Name, "/depends") {
			p.buildFromDBFile(tr)
			c++
		}
		if c < 2 {
			continue
		}
		r.Packages = append(r.Packages, p)
	}
	return nil
}

func (s *Searcher) searchDB(repo *Repo, searchString string, pacChan chan PackageList) {
	var formula func(string, string) bool
	if s.exact {
		formula = func(a, b string) bool {
			return a == b
		}
	} else {
		formula = strings.Contains
	}
	searchString = strings.ToLower(searchString)
	pkgs := make([]*Package, 0, 10)
Loop:
	for _, p := range repo.Packages {
		if s.byName {
			if formula(strings.ToLower(p.Name), searchString) {
				pkgs = append(pkgs, p)
				continue
			}
		}
		if s.byDesc {
			if formula(strings.ToLower(p.Desc), searchString) {
				pkgs = append(pkgs, p)
				continue

			}
		}
		if s.byBase {
			if formula(strings.ToLower(p.Base), searchString) {
				pkgs = append(pkgs, p)
				continue

			}
		}
		if s.byGroups {
			if p.Groups != nil {
				for _, v := range p.Groups {
					if formula(strings.ToLower(v), searchString) {
						pkgs = append(pkgs, p)
						continue Loop

					}
				}
			}
		}
		if s.byProvides {
			if p.Provides != nil {
				for _, v := range p.Provides {
					if formula(strings.ToLower(v.Name), searchString) {
						pkgs = append(pkgs, p)
						continue Loop
					}
				}
			}
		}
	}
	pacChan <- pkgs
}
