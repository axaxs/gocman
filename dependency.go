package main

import "fmt"

func (d *Dependency) Resolve() (PackageList, error) {
	ser := &Searcher{}
	ser.byName = true
	ser.exact = true
	pl := ser.searchForDependency(d.Name)
	if len(pl) > 0 {
		for i := range pl {
        		if d.Operator == "" || compVersions(pl[i].Version, d.Operator, d.Version) {
				return pl[i:i+1], nil
              		  }
		}
		return nil, fmt.Errorf("%s version %s %s needed, but %s found", d.Name, d.Operator, d.Version, pl[0].Version)
	}
	pl = ser.searchForProviders(d.Name)
	toRet := make(PackageList, 0, len(pl))
	for _,v := range pl {
		if d.Operator == "" || compVersions(v.Version, d.Operator, d.Version) {
			toRet = append(toRet, v)
		}
	}
	if len(toRet) > 0 {
		return toRet, nil
	}
	return nil, fmt.Errorf("Dependency %s cannot be found", d.Name)
}
		
		
func (d *Dependency) isInstalled() bool {
	for _, lp := range localDB {
		if d.Name == lp.Name {
			if d.Operator == "" || compVersions(lp.Version, d.Operator, d.Version) {
				return true
			}
		}
		for _, v := range lp.Provides {
			if d.Name == v.Name {
				if d.Operator == "" || compVersions(lp.Version, d.Operator, d.Version) {
					return true
				}
			}
		}
	}
	return false
}
