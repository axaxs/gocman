package main

import ("fmt";"net/http";"io";"os";)

type DLResult struct {
	packagename string
	err error
}

	
func (pl PackageList) Download() {
	reschan := make(chan DLResult)
	donechan := make(chan bool, 3)
	deathchan := make(chan bool, 4)
	for i:=0;i<3;i++ {
		donechan <- true
	}
	go func() {
	        for _,p := range pl {
			select {
			case <-deathchan:
				for i := 0; i<4; i++ {
					deathchan <- true
				}
				close(deathchan)
				return
			default:
	        	        <-donechan
	          	     	go p.Download(reschan, donechan, deathchan)
			}
       		 }
	}()
	for range pl {
		res := <-reschan
		if res.err != nil {
			deathchan<-true
			fmt.Printf("Error downloading %s - %s\n", res.packagename, res.err.Error())
			
		}
	}
}

func (p *Package) Download(reschan chan DLResult, donechan, deathchan chan bool) {
	rs := DLResult{p.Name, nil}
	defer func() { reschan <- rs 
			donechan <- true
	}()
	fi,err := os.Stat("/var/cache/pacman/" + p.Filename)
	if err != nil {
		if int(fi.Size()) == p.Csize {
			return
		}
		os.Remove("/var/cache/pacman/" + p.Filename)
	}	
	if len(p.Repo.MirrorList) == 0 {
		rs.err = fmt.Errorf("Could not find any mirrors for %s, please check pacman.conf file", p.Repo.Name)
		return
	}
	for _,m := range p.Repo.MirrorList {
		rs.err = nil
		os.Remove("/var/cache/pacman/." + p.Filename)
		resp, err := http.Get(m + "/" + p.Filename)
		if err != nil {
			rs.err = err
			continue
		}
		defer resp.Body.Close()
		f, err := os.Create("/var/cache/pacman/." + p.Filename)
		if err != nil {
			rs.err = err
			continue
		}
		b := make([]byte, 4096)
		total := 0
		for total < p.Csize {
			select {
			case <-deathchan:
				os.Remove("/var/cache/pacman/." + p.Filename)
				return
			default:
				n,err := resp.Body.Read(b)
				if err != nil && err != io.EOF {
					rs.err = err
					break
				}
				total += n
				_,err = f.Write(b[:n])
				if err != nil {
					rs.err = err
					break
				}	
			}
		}
		if rs.err != nil {
			continue
		}
		err = os.Rename("/var/cache/pacman/." + p.Filename, "/var/cache/pacman/" + p.Filename)
		if err != nil {
			rs.err = err
			continue
		}
		break
	}
}
