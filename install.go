package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	//lzma "github.com/gonuts/go-liblzma")
)

func getPackages(packageNames []string) (PackageList, error) {
	s := &Searcher{}
	res := make(PackageList, 0, 10)
	for _, v := range packageNames {
		r := s.searchByName(v)
		for len(r) == 0 {
			groupSearcher := &Searcher{}
			r = groupSearcher.searchByGroup(v)
			if len(r) > 0 {
				r = r.chooseGroup(v)
				break
			}
			fmt.Printf("Package %s not found.  Re-enter name or press enter to exit: ", v)
			reader := bufio.NewReader(os.Stdin)
			tryAgain, _ := reader.ReadString('\n')
			tryAgain = strings.TrimSpace(tryAgain)
			if tryAgain == "" {
				return nil, fmt.Errorf("error: target not found: %s", v)
			}
			v = tryAgain
			r = s.searchByName(v)
		}
		res = append(res, r...)
	}
	return res, nil
}


func (pl PackageList) providerChoose (name string) *Package  {
	for _,pkg := range pl {
		if localDB.containsPackage(pkg) {
			return nil
		}
	}
	for {
		fmt.Printf("Need package to satisfy %s\n", name)
	       	for i, pkk := range pl {
			fmt.Printf("%d) %s  ", i+1, pkk.Name)
		}
		rd := bufio.NewReader(os.Stdin)
	        v, _ := rd.ReadString('\n')
		chosen,_ := strconv.Atoi(v)
		for i, pkk := range pl {
			if chosen == i + 1 {
				return pkk
			}
		}
		fmt.Println("Invalid selection...")
	}
	return nil
}
					


func install(names []string) {
	pkgs, err := getPackages(names)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	pkgs = pkgs.trimInstalled()
	if len(pkgs) == 0 {
		fmt.Println("Nothing to do...")
		os.Exit(0)
	}
	fmt.Println("resolving dependencies...")
	for {
		provs, err := pkgs.GetDependencies()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		if len(provs) == 0 {
			break
		}
		for k,v := range provs {
			pkgs = append(pkgs, v.chooseProvider(k))
		}
	}
	fmt.Println("looking for inter-conflicts...\n")
	p1, p2, conflicts := pkgs.checkConflicts()
	if conflicts {
		fmt.Printf("Package %s conflicts with %s.  Exiting\n", p1.Name, p2.Name)
		os.Exit(1)
	}
	pkgs.Sort()
	dsize := 0
	isize := 0
	if len(pkgs) > 0 {
		lp := &LinePrinter{}
		lp.print(fmt.Sprintf("Packages (%d): ", len(pkgs)))
		lp.align = lp.curcount
		for _, v := range pkgs {
			dsize += v.Csize
			isize += v.Isize
			lp.print(fmt.Sprintf("%s-%s  ", v.Name, v.Version))
		}
	}
	fmt.Println("\n")
	fmt.Printf("Total Download Size:    %.2f MiB\n", float32(dsize)/(1024*1024))
	fmt.Printf("Total Installed Size:   %.2f MiB\n", float32(isize)/(1024*1024))
}
