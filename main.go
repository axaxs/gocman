package main

import (
	"fmt"
	"os"
	"runtime"
)

var mainConfig *Config

func printHelp() {
	fmt.Println("help")
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println(" -h for help")
		os.Exit(1)
	}
	var err error
	mainConfig, err = readConf()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	runtime.GOMAXPROCS(runtime.NumCPU())
	switch os.Args[1] {
	case "search":
		if len(os.Args) < 3 {
			fmt.Println("No search terms given")
			os.Exit(1)
		}
		buildRepos()
		search(os.Args[2:])
	case "install":
		if len(os.Args) < 3 {
			fmt.Println("No packages to install given")
			os.Exit(1)
		}
		buildRepos()
		install(os.Args[2:])
	default:
		fmt.Println("Unrecognized command")
	}
}
