package main

import (
	"bufio"
	"io"
	"strconv"
	"strings"
)

type Package struct {
        Filename  string
        Name      string
        Base      string
        Version   string
        Desc      string
        Groups    []string
        Csize     int
        Isize     int
        MD5sum    string
        SHA256sum string
        PGPsig    string
        URL       string
        License   []string
        Arch      string
        BuildDate int
        Packager  string
	Replaces []string
        Depends     []*Dependency
        Provides    []*Provide
        Conflicts   []string
        MakeDepends []*Dependency
	OptDepends []string
	Backup []string
	Repo    *Repo
}

type Dependency struct {
	Name       string
	Operator   string
	Version    string
	Package    *Package
	ProvidedBy PackageList
}

type Provide struct {
	Name    string
	Version string
}

func stringToDependency(in string) *Dependency {
	depy := &Dependency{}
	opFound := false
	for _, c := range in {
		switch x := string(c); x {
		case ">", "=", "<":
			depy.Operator += x
			opFound = true
		default:
			if opFound {
				depy.Version += x
			} else {
				depy.Name += x
			}
		}
	}
	return depy
}

func stringToProvide(in string) *Provide {
	prov := &Provide{}
	spT := strings.Split(in, "=")
	prov.Name = spT[0]
	if len(spT) > 1 {
		prov.Version = spT[1]
	}
	return prov
}

func (p *Package) dependsOn(p2 *Package) bool {
	for _,d := range p.Depends {
		if d.Name == p2.Name {
			if d.Operator == "" || compVersions(p2.Version, d.Operator, d.Version) {
				return true
			}
		}
		if p2.Provides != nil {
			for _,p := range p2.Provides {
				if p.Name == d.Name {
					if d.Operator == "" || compVersions(p.Version, d.Operator, d.Version) {
						return true
					}
				}
			}
		}
	}
	return false
}

func (p *Package) conflictsWith(p2 *Package) bool {
	if p.Conflicts != nil {
		for _, v := range p.Conflicts {
			if v == p2.Name {
				return true
			}
		}
	}
	if p2.Conflicts != nil {
		for _, v := range p2.Conflicts {
			if v == p.Name {
				return true
			}
		}
	}
	return false
}

func (p *Package) buildFromPackageFile(r io.Reader) {
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		text := sc.Text()
		if strings.HasPrefix(text, "#") {
			continue
		}
		spltext := strings.Split(text, "=")
		if len(spltext) < 2 {
			continue
		}
		if len(spltext) > 2 {
			spltext[1] = strings.Join(spltext[1:], "=")
		}
		k,v := strings.TrimSpace(spltext[0]), strings.TrimSpace(spltext[1])
		switch k {
		case "pkgname":
			p.Name = v
		case "pkgbase":
			p.Base = v
		case "pkgver":
			p.Version = v
		case "pkgdesc":
			p.Desc = v
		case "url":
			p.URL = v
		case "builddate":
			dt, err := strconv.Atoi(v)
			if err == nil {
				p.BuildDate = dt
			}
		case "packager":
			p.Packager = v
		case "size":
			sz,err := strconv.Atoi(v)
			if err == nil {
				p.Csize = sz
			}
		case "arch":
			p.Arch = v
		case "license":
			p.License = append(p.License, v)
		case "replaces":
			p.Replaces = append(p.Replaces, v)
		case "conflict":
			p.Conflicts = append(p.Conflicts, v)
		case "provides":
			p.Provides = append(p.Provides, stringToProvide(v))
		case "backup":
			p.Backup = append(p.Backup, v)
		case "depend":
			p.Depends = append(p.Depends, stringToDependency(v))
		case "optdepend":
			p.OptDepends = append(p.OptDepends, v)
		case "makedepend":
			p.MakeDepends = append(p.MakeDepends, stringToDependency(v))
		}
	}
}

func (p *Package) buildFromDBFile(r io.Reader) {
	sc := bufio.NewScanner(r)
	for sc.Scan() {
		text := sc.Text()
		if strings.HasPrefix(text, "%") && strings.HasSuffix(text, "%") {
			sc.Scan()
			switch strings.Trim(text, "%") {
			case "FILENAME":
				p.Filename = sc.Text()
			case "NAME":
				p.Name = sc.Text()
			case "BASE":
				p.Base = sc.Text()
			case "VERSION":
				p.Version = sc.Text()
			case "DESC":
				p.Desc = sc.Text()
			case "GROUPS":
				p.Groups = make([]string, 0, 2)
				for sc.Text() != "" {
					p.Groups = append(p.Groups, sc.Text())
					sc.Scan()
				}
			case "CSIZE":
				i, err := strconv.Atoi(sc.Text())
				if err != nil {
					continue
				}
				p.Csize = i
			case "ISIZE":
				i, err := strconv.Atoi(sc.Text())
				if err != nil {
					continue
				}
				p.Isize = i
			case "MD5SUM":
				p.MD5sum = sc.Text()
			case "SHA256SUM":
				p.SHA256sum = sc.Text()
			case "PGPSIG":
				p.PGPsig = sc.Text()
			case "URL":
				p.URL = sc.Text()
			case "LICENSE":
				p.License = append(p.License, sc.Text())
			case "ARCH":
				p.Arch = sc.Text()
			case "BUILDDATE":
				i, err := strconv.Atoi(sc.Text())
				if err == nil {
					p.BuildDate = i
				}
			case "PACKAGER":
				p.Packager = sc.Text()
                        case "DEPENDS":
                                for sc.Text() != "" {
                                        depy := stringToDependency(sc.Text())
                                        p.Depends = append(p.Depends, depy)
                                        sc.Scan()
                                }
                        case "PROVIDES":
                                for sc.Text() != "" {
                                        p.Provides = append(p.Provides, stringToProvide(sc.Text()))
                                        sc.Scan()
                                }
                        case "CONFLICTS":
                                for sc.Text() != "" {
                                        p.Conflicts = append(p.Conflicts, sc.Text())
                                        sc.Scan()
				}
                        case "MAKEDEPENDS":
                                for sc.Text() != "" {
                                        p.MakeDepends = append(p.MakeDepends, stringToDependency(sc.Text()))
                                        sc.Scan()
                                }
                        }

		}
	}
}
