package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type PackageList []*Package
type ProviderMap map[string]PackageList

func (pl PackageList) Sort() {
	changed := true
	for changed == true {
		changed = false
		for i:=0;i<len(pl);i++ {
			for j:=0;j<len(pl);j++ {
				if pl[i].dependsOn(pl[j]) {
					if pl[j].dependsOn(pl[i]) {
						continue
					}
					if i < j {
						pl[i], pl[j] = pl[j], pl[i]
						changed = true
						break
					}
				}
			}
		}
	}
}

func (pl *PackageList) GetDependencies() (ProviderMap, error) {
	checked := true
        checkedDependencies := make(map[string]bool)
	pm := make(ProviderMap)
        for checked == true {
                checked = false
                toAdd := make(PackageList, 0, 5)
                for _,v := range *pl {
		DepLoop:
                        for _,dep := range v.Depends {
                                if _,ok := checkedDependencies[dep.Name]; ok {
                                        continue
                                }
                                checked = true
                                checkedDependencies[dep.Name] = true
                                if dep.isInstalled() {
                                        continue
                                }
                                pkl, err := dep.Resolve()
                                if err != nil {
                                        return nil, fmt.Errorf("Error resolving dependency for %s - %s", v.Name, err.Error())
                                        os.Exit(1)
                                }
                                if len(pkl) > 1 {
                                        for _,pp := range pkl {
                                                if (*pl).containsPackage(pp) {
                                                        continue DepLoop
                                                }
                                        }
					pm[dep.Name] = pkl
                                } else if len(pkl) == 1 {
                                        if (*pl).containsPackage(pkl[0]) {
						continue
					}
					toAdd = append(toAdd, pkl[0])
                                }
                        }

                }
                *pl = append(toAdd, *pl...)
        }
	return pm, nil
}

func (pl PackageList) trimInstalled() PackageList {
	retPkg := make(PackageList,0,len(pl))
	for _,p := range pl {
		if !localDB.containsPackage(p) {
			retPkg = append(retPkg, p)
			continue
		}
		fmt.Printf("warning: %s-%s is up to date -- skipping\n", p.Name, p.Version)
	}
	return retPkg
}

func (pl PackageList) addPackage(p *Package) error {
	for _, pk := range append(pl, localDB...) {
		if pk.conflictsWith(p) {
			return fmt.Errorf("Package %s conflicts with %s", pk.Name, p.Name)
		}
	}
	return nil
}

func (pl PackageList) containsPackage(p *Package) bool {
	for _, v := range pl {
		if v.Name == p.Name {
			return true
		}
	}
	return false
}

func (pl PackageList) checkConflicts() (*Package, *Package, bool) {
	for _, v := range pl {
		for _, vv := range localDB {
			if vv.conflictsWith(v) {
				return vv, v, true
			}
		}
		for _, vv := range pl {
			if vv.conflictsWith(v) {
				return vv, v, true
			}
		}
	}
	return nil, nil, false
}

func (pl PackageList) chooseProvider(name string) *Package {
	for {
		fmt.Printf("Need package to satisfy %s\n", name)
		for i, pkk := range pl {
			fmt.Printf("%d) %s  ", i+1, pkk.Name)
		}
		fmt.Println("")
		fmt.Printf("Enter selection: ")
		rd := bufio.NewReader(os.Stdin)
		v, _ := rd.ReadString('\n')
		for i, pkk := range pl {
			chosen,_ := strconv.Atoi(strings.TrimSpace(v))
			if chosen == i+1 {
				return pkk
			}
		}
	}
}

func (pkgs PackageList) chooseGroup(groupName string) PackageList {
        res := make(PackageList, 0, 10)
        if len(pkgs) > 0 {
                gmap := make(map[int]*Package)
                fmt.Printf(":: There are %d members in group %s:\n:: Repository %s", len(pkgs), groupName, pkgs[0].Repo.Name)
                lp := &LinePrinter{80, 2, false}
                for i, pk := range pkgs {
                        lp.print(fmt.Sprintf(" %d) %s ", i+1, pk.Name))
                        gmap[i+1] = pk
                }
                fmt.Println("\n")
                fmt.Println("Enter corresponding number of packages to install, space separated.")
                fmt.Println("Leave blank to install all packages listed.")
                fmt.Println("To instead list packages to exclude, use ^#, space separated\n")
                fmt.Printf("Enter a selection (default=all): ")
                rd := bufio.NewReader(os.Stdin)
                v, _ := rd.ReadString('\n')
                sels := strings.Split(v, " ")
                igints := make([]int, 0, 5)
                addints := make([]int, 0, 5)
                for _, item := range sels {
                        item = strings.TrimSpace(item)
                        if len(item) == 0 {
                                continue
                        }
                        if item[0] == '^' {
                                if len(item) < 2 {
                                        continue
                                }
                                asInt, err := strconv.Atoi(item[1:])
                                if err != nil {
                                        fmt.Println(err)
                                        continue
                                }
                                igints = append(igints, asInt)
                        } else {
                                asInt, err := strconv.Atoi(item)
                                if err != nil {
                                        fmt.Println(err)
                                        continue
                                }
                                addints = append(addints, asInt)
                       }
                }
                if len(addints) == 0 {
                        for i := range pkgs {
                                add := true
                                for _, v := range igints {
                                        if v == i+1 {
                                                add = false
                                        }
                                }
                                if add {
                                        res = append(res, gmap[i+1])
                                }
                        }
                } else if len(addints) > 0 {
                        for _, a := range addints {
                                add := true
                                for _, v := range igints {
                                        if v == a {
                                                add = false
                                        }
                                }
                                if add {
                                        res = append(res, gmap[a])
                                }
                        }
                }
        }
        return res
}

