package main

import ("io/ioutil";"fmt";"strings";)


type Repo struct {
        Name     string
        Include  string
        Server   string
        SigLevel []string
        Packages []*Package
	MirrorList []string
}

func (r *Repo) GetMirrors() error {
	var err error
	var mlist []string
		arch := getArch()
		if r.Include != "" {
			mlist, err = r.getMirrorsFromFile(r.Include)
			if err != nil {
				return err
			}
		} else if r.Server != "" {
			mlist = []string{strings.Replace(strings.Replace(r.Server, "$arch", arch, -1), "$repo", r.Name, -1)}
		} else {
			return fmt.Errorf("No Include or Server given for repo %s", r.Name)
		}
		r.MirrorList = mlist
	return nil
}

func (r *Repo) getMirrorsFromFile(fname string) ([]string, error) {
	b, err := ioutil.ReadFile(fname)
	if err != nil {
		return []string{}, err
	}
	arch := getArch()
	splFile := strings.Split(string(b), "\n")
	mlist := make([]string,0,len(splFile))
	for _,v := range splFile {
		if strings.HasPrefix(v, "Server =") {
			mlist = append(mlist, strings.Replace(strings.Replace(strings.TrimSpace(strings.Split(v, "=")[1]), "$arch", arch, -1), "$repo", r.Name, -1))
		}
	}
	return mlist, nil
}

