package main

import (
	"fmt"
	"strings"
)

type Searcher struct {
	byName, byDesc, byBase, byGroups, byDepends, byProvides, exact bool
}

func (ser *Searcher) searchForDependency(name string) PackageList {
	ser.byName = true
	ser.byDesc = false
	ser.byBase = false
	ser.byGroups = false
	ser.byDepends = false
	ser.byProvides = false
	ser.exact = true
	return ser.search([]string{name})
}

func (ser *Searcher) searchForProviders(name string) PackageList {
	ser.byName = false
	ser.byDesc = false
	ser.byBase = false
	ser.byGroups = false
	ser.byDepends = false
	ser.byProvides = true
	ser.exact = true
	return ser.search([]string{name})
}

func (ser *Searcher) searchByName(name string) PackageList {
	ser.byName = true
	ser.byDesc = false
	ser.byBase = false
	ser.byGroups = false
	ser.byDepends = false
	ser.byProvides = false
	ser.exact = true
	return ser.search([]string{name})
}

func (ser *Searcher) searchByGroup(groupName string) PackageList {
	ser.byName = false
	ser.byDesc = false
	ser.byBase = false
	ser.byGroups = true
	ser.byDepends = false
	ser.byProvides = false
	ser.exact = true
	return ser.search([]string{groupName})
}

func (ser *Searcher) search(s []string) PackageList {
	res := make([]*Package, 0, 10)
	chanMap := make(map[string]chan PackageList)
	for _, v := range mainConfig.repos {
		chanMap[v.Name] = make(chan PackageList)
	}
	for _, v := range mainConfig.repos {
		for _, ss := range s {
			go ser.searchDB(v, ss, chanMap[v.Name])
		}

	}
	for _, v := range mainConfig.repos {
		for _ = range s {
			sl := <-chanMap[v.Name]
			for _, el := range sl {
				res = append(res, el)
			}
		}
	}
	return res
}

func search(s []string) {
	ser := &Searcher{true, true, false, true, true, true, false}
	result := ser.search(s)
	for _, el := range result {
		groupline := ""
		iline := ""
		if el.Groups != nil {
			groupline = " (" + strings.Join(el.Groups, " ") + ")"
		}
		for _, le := range localDB {
			if le.Name == el.Name {
				if le.Version == el.Version {
					iline = " [installed]"
				} else {
					iline = " [installed: " + le.Version + "]"
				}
			}
		}
		lp := &LinePrinter{0, 4, true}
		lp.print(fmt.Sprintf("%s/%s %s%s%s", el.Repo.Name, el.Name, el.Version, groupline, iline))
		lp.curcount = 81
		for _, word := range strings.Split(el.Desc, " ") {
			lp.print(fmt.Sprintf("%s ", word))
		}
		fmt.Println("")

	}

}
