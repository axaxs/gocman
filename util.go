package main

import ( "fmt"
	"strings"
	"runtime"
	)

type LinePrinter struct {
	curcount int
	align int
	ignoreSpace bool
}

func (l *LinePrinter) print(toPrint string) {
	totalcount := l.curcount+len(toPrint)
	if totalcount > 80 {
		if l.ignoreSpace && l.curcount + len(strings.TrimSpace(toPrint)) < 81 {
			toPrint = strings.TrimSpace(toPrint)
			fmt.Printf(toPrint)
			l.curcount += len(toPrint)
			return
		}
        	fmt.Printf("\n")
                spc := ""
                for c := 0; c < l.align; c++ {
                	spc += " "
                }
                l.curcount = len(spc)
                fmt.Printf(spc)
        }
        fmt.Printf(toPrint)
        l.curcount += len(toPrint)
}

func compVersions(a, op, b string) bool {
        a = strings.Split(a, "-")[0]
        b = strings.Split(b, "-")[0]
        if op == "=" {
                return a == b
        }
        sa := strings.Split(a, ".")
        sb := strings.Split(b, ".")
        for len(sa) < len(sb) {
                sa = append(sa, "0")
        }
        for len(sb) < len(sa) {
                sb = append(sb, "0")
        }
        lt := true
        gt := true
        eq := true
        for i := range sa {
                for len(sa[i]) < len(sb[i]) {
                        sa[i] = "0" + sa[i]
                }
                for len(sb[i]) < len(sa[i]) {
                        sb[i] = "0" + sb[i]
                }
                if sa[i] < sb[i] {
                        gt = false
                        lt = true
                        eq = false
                        break
                } else if sa[i] > sb[i] {
                        gt = true
                        lt = false
                        eq = false
                        break
                }
        }
        switch op {
        case ">":
                return gt
        case ">=":
                return gt || eq
        case "<":
                return lt
        case "<=":
                return lt || eq
        }
	return false
}

func getArch() string {
        if runtime.GOARCH == "amd64" {
                return "x86_64"
        } else {
                return "x86"
        }
}
